class LoginModel {
  bool value;
  Data data;
  int code;

  LoginModel({this.value, this.data, this.code});

  LoginModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  int id;
  String name;
  String mobile;
  String email;
  int cityId;
  String cityName;
  String status;
  String type;
  int code;
  String verification;
  String whatsapp;
  String image;
  String imageCover;
  String address;
  String token;

  Data(
      {this.id,
      this.name,
      this.mobile,
      this.email,
      this.cityId,
      this.cityName,
      this.status,
      this.type,
      this.code,
      this.verification,
      this.whatsapp,
      this.image,
      this.imageCover,
      this.address,
      this.token});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    mobile = json['mobile'];
    email = json['email'];
    cityId = json['city_id'];
    cityName = json['city_name'];
    status = json['status'];
    type = json['type'];
    code = json['code'];
    verification = json['verification'];
    whatsapp = json['whatsapp'];
    image = json['image'];
    imageCover = json['image_cover'];
    address = json['address'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['mobile'] = this.mobile;
    data['email'] = this.email;
    data['city_id'] = this.cityId;
    data['city_name'] = this.cityName;
    data['status'] = this.status;
    data['type'] = this.type;
    data['code'] = this.code;
    data['verification'] = this.verification;
    data['whatsapp'] = this.whatsapp;
    data['image'] = this.image;
    data['image_cover'] = this.imageCover;
    data['address'] = this.address;
    data['token'] = this.token;
    return data;
  }
}
