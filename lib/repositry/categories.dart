import 'dart:async';
import 'package:block_test/models/categories.dart';
import 'package:block_test/networking/api_provider.dart';

class CategoryRepository {
  NetWork _netWork = NetWork();

  Future<Categories> getCategories() async {
    final response = await _netWork.getData(url: '');
    return Categories.fromJson(response);
  }
}
