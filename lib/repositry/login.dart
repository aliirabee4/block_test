import 'package:block_test/models/login.dart';
import 'package:block_test/networking/api_provider.dart';
import 'package:dio/dio.dart';

class LoginRespositry {
  NetWork _netWork = NetWork();

  Future<LoginModel> userLogin(
      {String phone,
      String password,
      String deviceType,
      String deviceToken}) async {
    FormData _formData = FormData.fromMap({
      'phone': phone,
      'password': password,
      'device_token': deviceToken,
      'device_type': deviceType
    });
    final response = await _netWork.postData(url: 'login', formData: _formData);
    return LoginModel.fromJson(response);
  }
}
