import 'dart:async';
import 'package:block_test/models/categories.dart';
import 'package:block_test/networking/response.dart';
import 'package:block_test/repositry/categories.dart';

class CategoriesBloc {
  CategoryRepository _categoryRepository;
  StreamController _categoriesListController;

  StreamSink<Response<Categories>> get categoriesListSink =>
      _categoriesListController.sink;

  Stream<Response<Categories>> get categoriesListStream =>
      _categoriesListController.stream;

  ChuckCategoryBloc() {
    _categoriesListController = StreamController<Response<Categories>>();
    _categoryRepository = CategoryRepository();
    fetchCategories();
  }

  fetchCategories() async {
    categoriesListSink.add(Response.loading('Getting Chuck Categories.'));
    try {
      Categories categories = await _categoryRepository.getCategories();
      categoriesListSink.add(Response.completed(categories));
    } catch (e) {
      categoriesListSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _categoriesListController?.close();
  }
}
