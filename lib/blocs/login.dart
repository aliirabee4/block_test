import 'dart:async';

import 'package:block_test/models/login.dart';
import 'package:block_test/repositry/login.dart';
import 'package:block_test/networking/response.dart';

class LoginBlock {
  LoginRespositry _loginRespositry;
  StreamController _loginController;

  StreamSink<Response<LoginModel>> get loginSink => _loginController.sink;
  Stream<Response<LoginModel>> get loginStream => _loginController.stream;

  login() {
    _loginController = StreamController<Response<LoginModel>>();
    _loginRespositry = LoginRespositry();
    postLogin();
  }

  postLogin() async {
    loginSink.add(Response.loading('siging you in ...'));
    try {
      LoginModel loginModel = await _loginRespositry.userLogin();
      loginSink.add(Response.completed(loginModel));
    } catch (e) {
      loginSink.add(Response.error(e));
    }
  }

  dispose() {
    _loginController?.close();
  }
}
