import 'package:dio/dio.dart';
import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'custom_exception.dart';

class NetWork {
  final String _baseUrl = "";
  Dio dio = Dio();

  Future<dynamic> getData({String url, Map<String, dynamic> headers}) async {
    headers != null
        ? dio.options.headers = headers
        : dio.options.headers.clear();
    var jsonResponse;
    try {
      final response = await dio.get(_baseUrl + url);
      jsonResponse = _handelResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return jsonResponse;
  }

  Future<dynamic> postData(
      {String url, Map<String, dynamic> headers, FormData formData}) async {
    headers != null
        ? dio.options.headers = headers
        : dio.options.headers.clear();
    var jsonResponse;
    try {
      final response = await dio.post(
        _baseUrl + url,
        data: formData
      );
      jsonResponse = _handelResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return jsonResponse;
  }

  dynamic _handelResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.data.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.data.toString());
      case 401:
        throw BadRequestException(response.data.toString());
      case 403:
        throw UnauthorisedException(response.data.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
